/* JNPI 2013/2014
 * Skrzeszewska Joanna (js277648) i Lazarczyk Dawid (dl337614)
 * Zadanie nr 4 */
#ifndef HOLDING_H
#define HOLDING_H

#include <iostream>
#include <array>
#include <boost/operators.hpp>

// function for safe subtraction
constexpr unsigned int subtr(unsigned int a, unsigned int b) {
    return (a >= b) ? (a - b) : 0;
}

// functions for safe dividing
constexpr unsigned int div(unsigned int a, unsigned int b) {
    return (b) ? (a / b) : 0;
}

template<unsigned int acc, unsigned int hs, unsigned int exo>
struct Company {
    enum Kind : unsigned int { ACC = acc, HS = hs, EXO = exo };
};

template<class C>
struct is_company {
    static constexpr bool value = false;
};

template<unsigned int acc, unsigned int hs, unsigned int exo>
struct is_company<Company<acc, hs, exo> > {
    static constexpr bool value = true;
};

typedef Company<1, 0, 0> Accountancy;
typedef Company<0, 1, 0> Hunting_shop;
typedef Company<0, 0, 1> Exchange_office;

template<class C1, class C2>
struct add_comp {
    static_assert(is_company<C1>::value && is_company<C2>::value,
        "The parameters should have the company type.");
    typedef Company<C1::ACC + C2::ACC, C1::HS + C2::HS,
                    C1::EXO + C2::EXO> type;
};

template<class C1, class C2>
struct remove_comp {
    static_assert(is_company<C1>::value && is_company<C2>::value,
        "The parameters should have the company type.");
    typedef Company<subtr(C1::ACC, C2::ACC),
                    subtr(C1::HS, C2::HS),
                    subtr(C1::EXO, C2::EXO)> type;
};

template<class C, unsigned int n>
struct multiply_comp {
    static_assert(is_company<C>::value,
        "The parameter should have the company type.");
    typedef Company<C::ACC * n, C::HS * n, C::EXO * n> type;
};

template<class C, unsigned int n>
struct split_comp {
    static_assert(is_company<C>::value,
        "The parameter should have the company type.");
    typedef Company<div(C::ACC, n), div(C::HS, n), div(C::EXO, n)> type;
};

template<class C>
struct additive_expand_comp {
    typedef typename add_comp<C, Company<1, 1, 1> >::type type;
};

template<class C>
struct additive_rollup_comp {
    typedef typename remove_comp<C, Company<1, 1, 1> >::type type;
};

template<class C>
class Group
    : boost::additive<Group<C>
    , boost::dividable<Group<C>, unsigned int
    , boost::multipliable<Group<C>, unsigned int
      > > >
{
    static_assert(is_company<C>::value,
        "The parameter should have the company type.");
    private:
        static const unsigned int DEF_SIZE = 1,
                                  DEF_ACC_VAL = 15,
                                  DEF_HS_VAL = 150,
                                  DEF_EXO_VAL = 50;
        static const int N = 3; // amount of different kinds of firms
        // the values below are aliases for the indexes of firms
        static const int ACC = 0, HS = 1, EXO = 2;
        unsigned int size;
        // values of different kinds of firms
        std::array<unsigned int, N> values;
        // number of firms of the given kind in a single company
        unsigned int firm_num(unsigned int kind_of_firm) const;

    public:
        typedef C company_type;
        static company_type company;
        Group(unsigned int k = DEF_SIZE);
        Group(const Group &other) : size(other.size), values(other.values) {}

        void set_acc_val(unsigned int i) { values[ACC] = i; }
        void set_hs_val(unsigned int i) { values[HS] = i; }
        void set_exo_val(unsigned int i) { values[EXO] = i; }

        unsigned int get_size() const { return size; }
        unsigned int get_acc_val() const { return values[ACC]; }
        unsigned int get_hs_val() const { return values[HS]; }
        unsigned int get_exo_val() const { return values[EXO]; }
        unsigned int get_value() const;

        Group<C>& operator+=(const Group &other);
        Group<C>& operator-=(const Group &other);
        Group<C>& operator*=(unsigned int n);
        Group<C>& operator/=(unsigned int n);
};

template<class C>
Group<C>::Group(unsigned int k) : size(k) {
    values[ACC] = DEF_ACC_VAL;
    values[HS] = DEF_HS_VAL;
    values[EXO] = DEF_EXO_VAL;
}

template<class C>
unsigned int Group<C>::firm_num(unsigned int kind_of_firm) const {
    switch (kind_of_firm) {
        case ACC:
            return company.ACC;
        case HS:
            return company.HS;
        case EXO:
            return company.EXO;
        // that shold not happen but it's needed to compile without warnings
        default:
            return 0;
    }
}

template<class C>
unsigned int Group<C>::get_value() const {
    unsigned int company_value = 0;
    for (int i = 0; i < N; i++)
        company_value += values[i] * firm_num(i);
    return size * company_value;
}

template<class C>
Group<C>& Group<C>::operator+=(const Group<C> & other) {
    unsigned int final_size = size + other.size;
    for (int i = 0; i < N; i++)
        values[i] = div((values[i] * size + other.values[i] * other.size) *
                        firm_num(i), final_size * firm_num(i));
    size = final_size;
    return *this;
}

template<class C>
Group<C>& Group<C>::operator-=(const Group<C> & other) {
    unsigned int final_size = subtr(size, other.size);
    for (int i = 0; i < N; i++)
        values[i] = div(subtr(values[i] * size, other.values[i] * other.size) *
                        firm_num(i), final_size * firm_num(i));
    size = final_size;
    return *this;
}

template<class C>
Group<C>& Group<C>::operator*=(unsigned int n) {
    size *= n;
    for (int i = 0; i < N; i++)
        values[i] = div(values[i], n);
    return *this;
}

template<class C>
Group<C>& Group<C>::operator/=(unsigned int n) {
    size = div(size, n);
    for (int i = 0; i < N; i++)
        values[i] *= n;
    return *this;
}

template<class C, class T>
bool operator<=(const Group<C> &a, const Group<T> &b) {
    return a.company.HS * a.get_size() <= b.company.HS * b.get_size() &&
           a.company.EXO * a.get_size() <= b.company.EXO * b.get_size();
}

template<class C>
bool operator<=(const Group<C> &a, const Group<C> &b) {
    return a.get_size() <= b.get_size();
}

template<class C, class T>
bool operator>=(const Group<C> &a, const Group<T> &b) {
    return b <= a;
}

template<class C, class T>
bool operator==(const Group<C> &a, const Group<T> &b) {
    return a >= b && a <= b;
}

template<class C, class T>
bool operator!=(const Group<C> &a, const Group<T> &b) {
    return !(a == b);
}

template<class C, class T>
bool operator<(const Group<C> &a, const Group<T> &b) {
    return a <= b && !(a >= b);
}

template<class C, class T>
bool operator>(const Group<C> &a, const Group<T> &b) {
    return a >= b && !(a <= b);
}

template<class C>
std::ostream& operator<<(std::ostream& stream, const Group<C> &group) {
    stream << "Number of companies: " << group.get_size()
           << "; Value: " << group.get_value() << std::endl;
    stream << "Accountancies value: " << group.get_acc_val()
           << ", Hunting shops value: " << group.get_hs_val()
           << ", Exchange offices value: " << group.get_exo_val()
           << std::endl;
    stream << "Accountancies: " << group.company.ACC
           << ", Hunting shops: " << group.company.HS
           << ", Exchange offices: " << group.company.EXO
           << std::endl;
    return stream;
}

// Factory struct creating a Group of the demanded type of company from the
// Group of another type of company with the same values of firms and size.
template<class A>
struct Factory {
    template<class B>
    static const Group<A> produce(const Group<B> &other) {
        Group<A> instance(other.get_size());
        instance.set_acc_val(other.get_acc_val());
        instance.set_hs_val(other.get_hs_val());
        instance.set_exo_val(other.get_exo_val());
        return instance;
    }
};

// factor used in multiplying global functions below
const unsigned int MULTIPLY_F = 10;

template<class C>
const Group<typename additive_expand_comp<C>::type>
additive_expand_group(const Group<C> &g) {
    return Factory<typename additive_expand_comp<C>::type>::produce(g);
}

template<class C>
const Group<typename multiply_comp<C, MULTIPLY_F>::type>
multiplicative_expand_group(const Group<C> &g) {
    return Factory<typename multiply_comp<C, MULTIPLY_F>::type>::produce(g);
}

template<class C>
Group<typename additive_rollup_comp<C>::type> const
additive_rollup_group(Group<C> const &g) {
    return Factory<typename additive_rollup_comp<C>::type>::produce(g);
}

template<class C>
Group<typename split_comp<C, MULTIPLY_F>::type> const
multiplicative_rollup_group(Group<C> const &g) {
    return Factory<typename split_comp<C, MULTIPLY_F>::type>::produce(g);
}

template<class C1, class C2, class C3>
bool solve_auction(const Group<C1> &g1, const Group<C2> &g2,
                   const Group<C3> &g3) {
    return (g1 > g2 && g1 > g3) || (g2 > g1 && g2 > g3) ||
           (g3 > g1 && g3 > g2);
}

#endif
