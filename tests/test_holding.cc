/* Testy do zadania holding
 * wszystko co bylo na forum
 * 5.12.2013 o 15:36 */
// linie oznaczone FIXME maja wywolywac blad kompilacji
// jesli nie odpalamy w folderze private to nie trzeba zmienic
#include "../holding.hh" 
#include "../holding.hh" // #ifndef #ednif ?

#include <typeinfo>
#include <cassert>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <climits>

using namespace std;

typedef stringstream sstr;

void testy_dziwne_zalozenia_na_dzien_dobry() {
    cout << "Testy dziwne zalozenie:\t";
    // zdaje sie, ze nikt nie powiedzial, ze tak to mamy parametryzowac,
    // ale zdaje sie, ze na forum to pojawia sie w testach jako 
    // "niejawen" zalozenie
    assert(typeid(Company<1, 0, 0>) == typeid(Accountancy));
    assert(typeid(Company<0, 1, 0>) == typeid(Hunting_shop));
    assert(typeid(Company<0, 0, 1>) == typeid(Exchange_office));
    cout << "OK" << endl;
}

void testy_z_tresci() {
    cout << "Testy z tresci:\t";
    sstr out;
    typedef add_comp<Accountancy, Exchange_office>::type small_company;
    typedef add_comp<multiply_comp<Hunting_shop, 10>::type, multiply_comp<Exchange_office, 20>::type>::type money;
    typedef add_comp<multiply_comp<Hunting_shop, 30>::type, multiply_comp<Exchange_office, 10>::type>::type guns;

  Group<Accountancy> s1;
  Group<money> s2(20);
  Group<money> s3(20);
  Group<Exchange_office> s4;
  Group<guns> s5(20);

  assert((s2 + s3).get_size() == 40);
  assert((s2 += s2).get_size() == 40);
  assert(s2.get_size() == 40);
  assert((s2 - s3).get_size() == 20);
  assert((s3 - s2).get_size() == 0);

  s3.set_exo_val(25);

  out << s2;
  out << s3;
  out << s2 + s3;
  out << s2 - s3;
  out << s2 * 4;
  out << s2 / 2;

  assert((s2 + s3).get_value() == 139200);
  assert((s2 - s3).get_value() == 60000);
  assert((s2 * 4).get_value() == 97600);
  assert((s2 / 2).get_value() == 100000);

  assert(s2 > s3);
  assert(s1 != s4);
  assert(s1 == s1);

  assert(typeid(add_comp<Accountancy, Exchange_office>::type) == typeid(small_company));
  assert(typeid(remove_comp<small_company, Accountancy>::type) == typeid(Exchange_office));
  assert(typeid(split_comp<small_company, 2>::type) == typeid(Company<0, 0, 0>));
  assert(typeid(additive_rollup_comp<multiply_comp<small_company, 2>::type>::type) ==
         typeid(small_company));
  assert(typeid(additive_expand_comp<small_company>::type) ==
         typeid(add_comp<Hunting_shop, multiply_comp<small_company, 2>::type>::type));

  assert(typeid(Group<add_comp<Accountancy, Exchange_office>::type>::company_type) ==
         typeid(small_company));
  assert(typeid(additive_expand_group(s1).company) ==
         typeid(add_comp<multiply_comp<Accountancy, 2>::type,
                add_comp<Hunting_shop, Exchange_office>::type>::type));
  assert(typeid(additive_rollup_group(s1).company) == typeid(Company<0,0,0>));
  assert(typeid(multiplicative_expand_group(s1).company) ==
         typeid(multiply_comp<Accountancy, 10>::type));
  assert(typeid(multiplicative_rollup_group(s5).company) ==
         typeid(add_comp<multiply_comp<Hunting_shop, 3>::type, Exchange_office>::type));

  assert(solve_auction(s1, s2, s5) == false);

//Powyższy przykład wypisuje na standardowe wyjście:

    stringstream exp;
// Poprawiony output 
/*
 * 
Odp: Zadanie 4
Bartosz Rybicki w dniu Monday, 2 December 2013, 09:16 napisał(a)
 
Niestety w przykładzie zamieszczonym pod zadaniem znalazł się pewien błąd, poprawny output wygląda w sposób następujący: 
*/
    exp << "Number of companies: 40; Value: 100000" << endl;
    exp << "Accountancies value: 0, Hunting shops value: 150, Exchange offices value: 50" << endl;
    exp << "Accountancies: 0, Hunting shops: 10, Exchange offices: 20" << endl;
    exp << "Number of companies: 20; Value: 40000" << endl;
    exp << "Accountancies value: 15, Hunting shops value: 150, Exchange offices value: 25" << endl;
    exp << "Accountancies: 0, Hunting shops: 10, Exchange offices: 20" << endl;
    exp << "Number of companies: 60; Value: 139200" << endl;
    exp << "Accountancies value: 0, Hunting shops value: 150, Exchange offices value: 41" << endl;
    exp << "Accountancies: 0, Hunting shops: 10, Exchange offices: 20" << endl;
    exp << "Number of companies: 20; Value: 60000" << endl;
    exp << "Accountancies value: 0, Hunting shops value: 150, Exchange offices value: 75" << endl;
    exp << "Accountancies: 0, Hunting shops: 10, Exchange offices: 20" << endl;
    exp << "Number of companies: 160; Value: 97600" << endl;
    exp << "Accountancies value: 0, Hunting shops value: 37, Exchange offices value: 12" << endl;
    exp << "Accountancies: 0, Hunting shops: 10, Exchange offices: 20" << endl;
    exp << "Number of companies: 20; Value: 100000" << endl;
    exp << "Accountancies value: 0, Hunting shops value: 300, Exchange offices value: 100" << endl;
    exp << "Accountancies: 0, Hunting shops: 10, Exchange offices: 20" << endl;

//  cout << exp.str() << endl << out.str() << endl;
    assert(exp.str() == out.str());


    cout << "OK" << endl;
}

void testy_z_forum() {
    cout << "Testy z forum:\t";

// Sunday, 1 December 2013, 18:04 
    {
        Group<Accountancy> g1;
        Group<Hunting_shop> g2;
        Group<Exchange_office> g3;
//        g1 += g2; // FIXME Dodawanie grup roznych typow
    }

// Monday, 2 December 2013, 13:34
    {

     assert(typeid(Company<1, 0, 0>) == typeid(Accountancy));
    Group<Company<0,1,1>>a;
    Group<Company<0,2,1>>b;
    Group<Company<0,1,1>>c;
    Group<Company<0,2,2>>d;
     
    assert(a<b);
    assert(!(a==b));
    assert(a<=b);
    assert(a<=c);
    assert(a<=d);
    }

// Saturday, 30 November 2013, 10:01
    {
    stringstream out, exp;
     //1. Jeśli chodzi o multipliy_comp to powinny zachodzić następujące równości:

    typeid(multiply_comp<Hunting_shop,1>::type)==typeid(Company<0,1,0>);
    typeid(multiply_comp<Hunting_shop,0>::type)==typeid(Company<0,0,0>);
    //2. Jeśli chodzi o split_comp to powinny zachodzić następujące równości:
    typeid(split_comp<Hunting_shop,1>::type)==typeid(Company<0,1,0>);
    typeid(split_comp<Hunting_shop,0>::type)==typeid(Company<0,0,0>);
    //3. Następujący kod:
    typedef Company<0,2,2> one;
    typedef Company<1,1,1> two;
     
    typedef remove_comp<one,two>::type one_two;
    Group<one_two>rc(1);
    out<<rc;
    //Wypisuje na standardowe wyjście: 
    exp << "Number of companies: 1; Value: 200" << endl;
    exp << "Accountancies value: 15, Hunting shops value: 150, Exchange offices value: 50" << endl;
    exp << "Accountancies: 0, Hunting shops: 1, Exchange offices: 1" << endl;
//    cout << exp.str() << endl << out.str() << endl;
    assert(exp.str() == out.str());
    //4. Mają zachodzić następujące nierówności:

    typeid(additive_rollup_comp<Company<2,1,1>>::type)==typeid(Company<1,0,0>);
    typeid(remove_comp<Company<2,1,1>,Company<1,1,1>>::type)==typeid(Company<1,0,0>);
    }

// Saturday, 30 November 2013, 05:41
    {
        Group<Accountancy> s1(20);
        Group<Accountancy> s2(10);
        s1.set_acc_val(10);
        s2.set_acc_val(100);
        auto s3 = (s1 - s2);
        assert(s3.get_acc_val() == 0);
    }
//  Saturday, 30 November 2013, 20:19
    {
        Group<Exchange_office> a;
        Group<Hunting_shop> b;
        assert((a < b) == false);
        assert((a > b) == false);
        assert((a == b) == false);
        assert((a != b) == true);
    }
// Saturday, 30 November 2013, 21:51 
    {
        stringstream out, exp;
    typedef add_comp<multiply_comp<Hunting_shop, 10>::type, multiply_comp<Exchange_office, 20>::type>::type money;

    Group<money> s2(20);
    Group<money> s3(20);

    s2.set_acc_val(20);
    s3.set_acc_val(30);

    out << (s2 + s3).get_acc_val() << endl << (s2 + s3);
    exp << 0 << endl
 << "Number of companies: 40; Value: 100000" << endl
 << "Accountancies value: 0, Hunting shops value: 150, Exchange offices value: 50" << endl
        <<"Accountancies: 0, Hunting shops: 10, Exchange offices: 20" << endl; 
//    cout << out.str() << " " << exp.str() << endl;
    assert(exp.str() == out.str());    
}
// Monday, 2 December 2013, 12:45 
{

    typedef add_comp<Accountancy, Exchange_office>::type small_company;
    typedef multiply_comp<small_company, 1000>::type big_company;

    Group<big_company> s0(100);
    s0.set_exo_val(UINT_MAX/100000);
    s0.set_acc_val(0);
    assert(s0.get_value() == 4294900000);//jeszcze nie overflow
    s0.set_acc_val(1);
    assert(s0.get_value() == 32704);//na students jest to overflow
}

// Wednesday, 4 December 2013, 18:28
{
    Group<Company<1, 0, 0> > a(20);
    Group<Company<1, 0, 0> > b(10);
    assert(!(a < b));
    assert(!(a <= b));
    assert(a > b);
    assert(a >= b);
    assert(!(a == b));
}

// Thursday, 5 December 2013
{
    Group<Company<1,0,0>>a(20);
    Group<Company<2,0,0>>b(10);
     
    assert(!(a<b));
    assert(a<=b);
    assert(!(a>b));
    assert(a>=b);
    assert(a==b);
}
//  Wednesday, 4 December 2013, 22:25
{ 
    Group<Exchange_office>a(20);
    assert((a * 0).get_exo_val() == 0);
    assert((a * 0).get_size() == 0);
    assert((a / 0).get_exo_val() == 0);
    assert((a / 0).get_size() == 0);
}

    cout << "OK" << endl;

}

void testy_z_paczki();


int main() {
    testy_dziwne_zalozenia_na_dzien_dobry();
    testy_z_tresci();
    testy_z_forum();
    testy_z_paczki();

    cout << "Wszystko OK" << endl;
    return 0;
}

void testy_z_paczki() {
    stringstream out, exp;
    cout << "Testy z paczki\t";
#define G(a,b,c) Group<Company<a,b,c>>
#define P(a) out << (a);
#define PP(a,b) P((a)<(b));P((a)>(b));P((a)<=(b));P((a)>=(b));P((a)!=(b));P((a)==(b));

G(0,0,0) g000;
G(0,0,1) g001;
G(0,0,2) g002;
G(0,1,0) g010;
G(0,1,1) g011;
G(0,1,2) g012;
G(0,2,0) g020;
G(0,2,1) g021;
G(0,2,2) g022;
G(1,0,0) g100;
G(1,0,1) g101;
G(1,0,2) g102;
G(1,1,0) g110;
G(1,1,1) g111;
G(1,1,2) g112;
G(1,2,0) g120;
G(1,2,1) g121;
G(1,2,2) g122;
G(2,0,0) g200;
G(2,0,1) g201;
G(2,0,2) g202;
G(2,1,0) g210;
G(2,1,1) g211;
G(2,1,2) g212;
G(2,2,0) g220;
G(2,2,1) g221;
G(2,2,2) g222;
PP(g000,g000);
PP(g000,g001);
PP(g000,g002);
PP(g000,g010);
PP(g000,g011);
PP(g000,g012);
PP(g000,g020);
PP(g000,g021);
PP(g000,g022);
PP(g000,g100);
PP(g000,g101);
PP(g000,g102);
PP(g000,g110);
PP(g000,g111);
PP(g000,g112);
PP(g000,g120);
PP(g000,g121);
PP(g000,g122);
PP(g000,g200);
PP(g000,g201);
PP(g000,g202);
PP(g000,g210);
PP(g000,g211);
PP(g000,g212);
PP(g000,g220);
PP(g000,g221);
PP(g000,g222);
PP(g001,g000);
PP(g001,g001);
PP(g001,g002);
PP(g001,g010);
PP(g001,g011);
PP(g001,g012);
PP(g001,g020);
PP(g001,g021);
PP(g001,g022);
PP(g001,g100);
PP(g001,g101);
PP(g001,g102);
PP(g001,g110);
PP(g001,g111);
PP(g001,g112);
PP(g001,g120);
PP(g001,g121);
PP(g001,g122);
PP(g001,g200);
PP(g001,g201);
PP(g001,g202);
PP(g001,g210);
PP(g001,g211);
PP(g001,g212);
PP(g001,g220);
PP(g001,g221);
PP(g001,g222);
PP(g002,g000);
PP(g002,g001);
PP(g002,g002);
PP(g002,g010);
PP(g002,g011);
PP(g002,g012);
PP(g002,g020);
PP(g002,g021);
PP(g002,g022);
PP(g002,g100);
PP(g002,g101);
PP(g002,g102);
PP(g002,g110);
PP(g002,g111);
PP(g002,g112);
PP(g002,g120);
PP(g002,g121);
PP(g002,g122);
PP(g002,g200);
PP(g002,g201);
PP(g002,g202);
PP(g002,g210);
PP(g002,g211);
PP(g002,g212);
PP(g002,g220);
PP(g002,g221);
PP(g002,g222);
PP(g010,g000);
PP(g010,g001);
PP(g010,g002);
PP(g010,g010);
PP(g010,g011);
PP(g010,g012);
PP(g010,g020);
PP(g010,g021);
PP(g010,g022);
PP(g010,g100);
PP(g010,g101);
PP(g010,g102);
PP(g010,g110);
PP(g010,g111);
PP(g010,g112);
PP(g010,g120);
PP(g010,g121);
PP(g010,g122);
PP(g010,g200);
PP(g010,g201);
PP(g010,g202);
PP(g010,g210);
PP(g010,g211);
PP(g010,g212);
PP(g010,g220);
PP(g010,g221);
PP(g010,g222);
PP(g011,g000);
PP(g011,g001);
PP(g011,g002);
PP(g011,g010);
PP(g011,g011);
PP(g011,g012);
PP(g011,g020);
PP(g011,g021);
PP(g011,g022);
PP(g011,g100);
PP(g011,g101);
PP(g011,g102);
PP(g011,g110);
PP(g011,g111);
PP(g011,g112);
PP(g011,g120);
PP(g011,g121);
PP(g011,g122);
PP(g011,g200);
PP(g011,g201);
PP(g011,g202);
PP(g011,g210);
PP(g011,g211);
PP(g011,g212);
PP(g011,g220);
PP(g011,g221);
PP(g011,g222);
PP(g012,g000);
PP(g012,g001);
PP(g012,g002);
PP(g012,g010);
PP(g012,g011);
PP(g012,g012);
PP(g012,g020);
PP(g012,g021);
PP(g012,g022);
PP(g012,g100);
PP(g012,g101);
PP(g012,g102);
PP(g012,g110);
PP(g012,g111);
PP(g012,g112);
PP(g012,g120);
PP(g012,g121);
PP(g012,g122);
PP(g012,g200);
PP(g012,g201);
PP(g012,g202);
PP(g012,g210);
PP(g012,g211);
PP(g012,g212);
PP(g012,g220);
PP(g012,g221);
PP(g012,g222);
PP(g020,g000);
PP(g020,g001);
PP(g020,g002);
PP(g020,g010);
PP(g020,g011);
PP(g020,g012);
PP(g020,g020);
PP(g020,g021);
PP(g020,g022);
PP(g020,g100);
PP(g020,g101);
PP(g020,g102);
PP(g020,g110);
PP(g020,g111);
PP(g020,g112);
PP(g020,g120);
PP(g020,g121);
PP(g020,g122);
PP(g020,g200);
PP(g020,g201);
PP(g020,g202);
PP(g020,g210);
PP(g020,g211);
PP(g020,g212);
PP(g020,g220);
PP(g020,g221);
PP(g020,g222);
PP(g021,g000);
PP(g021,g001);
PP(g021,g002);
PP(g021,g010);
PP(g021,g011);
PP(g021,g012);
PP(g021,g020);
PP(g021,g021);
PP(g021,g022);
PP(g021,g100);
PP(g021,g101);
PP(g021,g102);
PP(g021,g110);
PP(g021,g111);
PP(g021,g112);
PP(g021,g120);
PP(g021,g121);
PP(g021,g122);
PP(g021,g200);
PP(g021,g201);
PP(g021,g202);
PP(g021,g210);
PP(g021,g211);
PP(g021,g212);
PP(g021,g220);
PP(g021,g221);
PP(g021,g222);
PP(g022,g000);
PP(g022,g001);
PP(g022,g002);
PP(g022,g010);
PP(g022,g011);
PP(g022,g012);
PP(g022,g020);
PP(g022,g021);
PP(g022,g022);
PP(g022,g100);
PP(g022,g101);
PP(g022,g102);
PP(g022,g110);
PP(g022,g111);
PP(g022,g112);
PP(g022,g120);
PP(g022,g121);
PP(g022,g122);
PP(g022,g200);
PP(g022,g201);
PP(g022,g202);
PP(g022,g210);
PP(g022,g211);
PP(g022,g212);
PP(g022,g220);
PP(g022,g221);
PP(g022,g222);
PP(g100,g000);
PP(g100,g001);
PP(g100,g002);
PP(g100,g010);
PP(g100,g011);
PP(g100,g012);
PP(g100,g020);
PP(g100,g021);
PP(g100,g022);
PP(g100,g100);
PP(g100,g101);
PP(g100,g102);
PP(g100,g110);
PP(g100,g111);
PP(g100,g112);
PP(g100,g120);
PP(g100,g121);
PP(g100,g122);
PP(g100,g200);
PP(g100,g201);
PP(g100,g202);
PP(g100,g210);
PP(g100,g211);
PP(g100,g212);
PP(g100,g220);
PP(g100,g221);
PP(g100,g222);
PP(g101,g000);
PP(g101,g001);
PP(g101,g002);
PP(g101,g010);
PP(g101,g011);
PP(g101,g012);
PP(g101,g020);
PP(g101,g021);
PP(g101,g022);
PP(g101,g100);
PP(g101,g101);
PP(g101,g102);
PP(g101,g110);
PP(g101,g111);
PP(g101,g112);
PP(g101,g120);
PP(g101,g121);
PP(g101,g122);
PP(g101,g200);
PP(g101,g201);
PP(g101,g202);
PP(g101,g210);
PP(g101,g211);
PP(g101,g212);
PP(g101,g220);
PP(g101,g221);
PP(g101,g222);
PP(g102,g000);
PP(g102,g001);
PP(g102,g002);
PP(g102,g010);
PP(g102,g011);
PP(g102,g012);
PP(g102,g020);
PP(g102,g021);
PP(g102,g022);
PP(g102,g100);
PP(g102,g101);
PP(g102,g102);
PP(g102,g110);
PP(g102,g111);
PP(g102,g112);
PP(g102,g120);
PP(g102,g121);
PP(g102,g122);
PP(g102,g200);
PP(g102,g201);
PP(g102,g202);
PP(g102,g210);
PP(g102,g211);
PP(g102,g212);
PP(g102,g220);
PP(g102,g221);
PP(g102,g222);
PP(g110,g000);
PP(g110,g001);
PP(g110,g002);
PP(g110,g010);
PP(g110,g011);
PP(g110,g012);
PP(g110,g020);
PP(g110,g021);
PP(g110,g022);
PP(g110,g100);
PP(g110,g101);
PP(g110,g102);
PP(g110,g110);
PP(g110,g111);
PP(g110,g112);
PP(g110,g120);
PP(g110,g121);
PP(g110,g122);
PP(g110,g200);
PP(g110,g201);
PP(g110,g202);
PP(g110,g210);
PP(g110,g211);
PP(g110,g212);
PP(g110,g220);
PP(g110,g221);
PP(g110,g222);
PP(g111,g000);
PP(g111,g001);
PP(g111,g002);
PP(g111,g010);
PP(g111,g011);
PP(g111,g012);
PP(g111,g020);
PP(g111,g021);
PP(g111,g022);
PP(g111,g100);
PP(g111,g101);
PP(g111,g102);
PP(g111,g110);
PP(g111,g111);
PP(g111,g112);
PP(g111,g120);
PP(g111,g121);
PP(g111,g122);
PP(g111,g200);
PP(g111,g201);
PP(g111,g202);
PP(g111,g210);
PP(g111,g211);
PP(g111,g212);
PP(g111,g220);
PP(g111,g221);
PP(g111,g222);
PP(g112,g000);
PP(g112,g001);
PP(g112,g002);
PP(g112,g010);
PP(g112,g011);
PP(g112,g012);
PP(g112,g020);
PP(g112,g021);
PP(g112,g022);
PP(g112,g100);
PP(g112,g101);
PP(g112,g102);
PP(g112,g110);
PP(g112,g111);
PP(g112,g112);
PP(g112,g120);
PP(g112,g121);
PP(g112,g122);
PP(g112,g200);
PP(g112,g201);
PP(g112,g202);
PP(g112,g210);
PP(g112,g211);
PP(g112,g212);
PP(g112,g220);
PP(g112,g221);
PP(g112,g222);
PP(g120,g000);
PP(g120,g001);
PP(g120,g002);
PP(g120,g010);
PP(g120,g011);
PP(g120,g012);
PP(g120,g020);
PP(g120,g021);
PP(g120,g022);
PP(g120,g100);
PP(g120,g101);
PP(g120,g102);
PP(g120,g110);
PP(g120,g111);
PP(g120,g112);
PP(g120,g120);
PP(g120,g121);
PP(g120,g122);
PP(g120,g200);
PP(g120,g201);
PP(g120,g202);
PP(g120,g210);
PP(g120,g211);
PP(g120,g212);
PP(g120,g220);
PP(g120,g221);
PP(g120,g222);
PP(g121,g000);
PP(g121,g001);
PP(g121,g002);
PP(g121,g010);
PP(g121,g011);
PP(g121,g012);
PP(g121,g020);
PP(g121,g021);
PP(g121,g022);
PP(g121,g100);
PP(g121,g101);
PP(g121,g102);
PP(g121,g110);
PP(g121,g111);
PP(g121,g112);
PP(g121,g120);
PP(g121,g121);
PP(g121,g122);
PP(g121,g200);
PP(g121,g201);
PP(g121,g202);
PP(g121,g210);
PP(g121,g211);
PP(g121,g212);
PP(g121,g220);
PP(g121,g221);
PP(g121,g222);
PP(g122,g000);
PP(g122,g001);
PP(g122,g002);
PP(g122,g010);
PP(g122,g011);
PP(g122,g012);
PP(g122,g020);
PP(g122,g021);
PP(g122,g022);
PP(g122,g100);
PP(g122,g101);
PP(g122,g102);
PP(g122,g110);
PP(g122,g111);
PP(g122,g112);
PP(g122,g120);
PP(g122,g121);
PP(g122,g122);
PP(g122,g200);
PP(g122,g201);
PP(g122,g202);
PP(g122,g210);
PP(g122,g211);
PP(g122,g212);
PP(g122,g220);
PP(g122,g221);
PP(g122,g222);
PP(g200,g000);
PP(g200,g001);
PP(g200,g002);
PP(g200,g010);
PP(g200,g011);
PP(g200,g012);
PP(g200,g020);
PP(g200,g021);
PP(g200,g022);
PP(g200,g100);
PP(g200,g101);
PP(g200,g102);
PP(g200,g110);
PP(g200,g111);
PP(g200,g112);
PP(g200,g120);
PP(g200,g121);
PP(g200,g122);
PP(g200,g200);
PP(g200,g201);
PP(g200,g202);
PP(g200,g210);
PP(g200,g211);
PP(g200,g212);
PP(g200,g220);
PP(g200,g221);
PP(g200,g222);
PP(g201,g000);
PP(g201,g001);
PP(g201,g002);
PP(g201,g010);
PP(g201,g011);
PP(g201,g012);
PP(g201,g020);
PP(g201,g021);
PP(g201,g022);
PP(g201,g100);
PP(g201,g101);
PP(g201,g102);
PP(g201,g110);
PP(g201,g111);
PP(g201,g112);
PP(g201,g120);
PP(g201,g121);
PP(g201,g122);
PP(g201,g200);
PP(g201,g201);
PP(g201,g202);
PP(g201,g210);
PP(g201,g211);
PP(g201,g212);
PP(g201,g220);
PP(g201,g221);
PP(g201,g222);
PP(g202,g000);
PP(g202,g001);
PP(g202,g002);
PP(g202,g010);
PP(g202,g011);
PP(g202,g012);
PP(g202,g020);
PP(g202,g021);
PP(g202,g022);
PP(g202,g100);
PP(g202,g101);
PP(g202,g102);
PP(g202,g110);
PP(g202,g111);
PP(g202,g112);
PP(g202,g120);
PP(g202,g121);
PP(g202,g122);
PP(g202,g200);
PP(g202,g201);
PP(g202,g202);
PP(g202,g210);
PP(g202,g211);
PP(g202,g212);
PP(g202,g220);
PP(g202,g221);
PP(g202,g222);
PP(g210,g000);
PP(g210,g001);
PP(g210,g002);
PP(g210,g010);
PP(g210,g011);
PP(g210,g012);
PP(g210,g020);
PP(g210,g021);
PP(g210,g022);
PP(g210,g100);
PP(g210,g101);
PP(g210,g102);
PP(g210,g110);
PP(g210,g111);
PP(g210,g112);
PP(g210,g120);
PP(g210,g121);
PP(g210,g122);
PP(g210,g200);
PP(g210,g201);
PP(g210,g202);
PP(g210,g210);
PP(g210,g211);
PP(g210,g212);
PP(g210,g220);
PP(g210,g221);
PP(g210,g222);
PP(g211,g000);
PP(g211,g001);
PP(g211,g002);
PP(g211,g010);
PP(g211,g011);
PP(g211,g012);
PP(g211,g020);
PP(g211,g021);
PP(g211,g022);
PP(g211,g100);
PP(g211,g101);
PP(g211,g102);
PP(g211,g110);
PP(g211,g111);
PP(g211,g112);
PP(g211,g120);
PP(g211,g121);
PP(g211,g122);
PP(g211,g200);
PP(g211,g201);
PP(g211,g202);
PP(g211,g210);
PP(g211,g211);
PP(g211,g212);
PP(g211,g220);
PP(g211,g221);
PP(g211,g222);
PP(g212,g000);
PP(g212,g001);
PP(g212,g002);
PP(g212,g010);
PP(g212,g011);
PP(g212,g012);
PP(g212,g020);
PP(g212,g021);
PP(g212,g022);
PP(g212,g100);
PP(g212,g101);
PP(g212,g102);
PP(g212,g110);
PP(g212,g111);
PP(g212,g112);
PP(g212,g120);
PP(g212,g121);
PP(g212,g122);
PP(g212,g200);
PP(g212,g201);
PP(g212,g202);
PP(g212,g210);
PP(g212,g211);
PP(g212,g212);
PP(g212,g220);
PP(g212,g221);
PP(g212,g222);
PP(g220,g000);
PP(g220,g001);
PP(g220,g002);
PP(g220,g010);
PP(g220,g011);
PP(g220,g012);
PP(g220,g020);
PP(g220,g021);
PP(g220,g022);
PP(g220,g100);
PP(g220,g101);
PP(g220,g102);
PP(g220,g110);
PP(g220,g111);
PP(g220,g112);
PP(g220,g120);
PP(g220,g121);
PP(g220,g122);
PP(g220,g200);
PP(g220,g201);
PP(g220,g202);
PP(g220,g210);
PP(g220,g211);
PP(g220,g212);
PP(g220,g220);
PP(g220,g221);
PP(g220,g222);
PP(g221,g000);
PP(g221,g001);
PP(g221,g002);
PP(g221,g010);
PP(g221,g011);
PP(g221,g012);
PP(g221,g020);
PP(g221,g021);
PP(g221,g022);
PP(g221,g100);
PP(g221,g101);
PP(g221,g102);
PP(g221,g110);
PP(g221,g111);
PP(g221,g112);
PP(g221,g120);
PP(g221,g121);
PP(g221,g122);
PP(g221,g200);
PP(g221,g201);
PP(g221,g202);
PP(g221,g210);
PP(g221,g211);
PP(g221,g212);
PP(g221,g220);
PP(g221,g221);
PP(g221,g222);
PP(g222,g000);
PP(g222,g001);
PP(g222,g002);
PP(g222,g010);
PP(g222,g011);
PP(g222,g012);
PP(g222,g020);
PP(g222,g021);
PP(g222,g022);
PP(g222,g100);
PP(g222,g101);
PP(g222,g102);
PP(g222,g110);
PP(g222,g111);
PP(g222,g112);
PP(g222,g120);
PP(g222,g121);
PP(g222,g122);
PP(g222,g200);
PP(g222,g201);
PP(g222,g202);
PP(g222,g210);
PP(g222,g211);
PP(g222,g212);
PP(g222,g220);
PP(g222,g221);
PP(g222,g222);
//cout << out.str();
exp << "001101101010101010101010101010101010101010101010101010001101101010101010101010101010101010101010101010101010001101101010101010101010101010101010101010101010101010010110001101101010000010101010101010000010101010101010010110001101101010000010101010101010000010101010101010010110001101101010000010101010101010000010101010101010010110010110001101000010000010101010000010000010101010010110010110001101000010000010101010000010000010101010010110010110001101000010000010101010000010000010101010010110000010000010001101101010101010101010101010101010010110000010000010001101101010101010101010101010101010010110000010000010001101101010101010101010101010101010010110010110000010010110001101101010000010101010101010010110010110000010010110001101101010000010101010101010010110010110000010010110001101101010000010101010101010010110010110010110010110010110001101000010000010101010010110010110010110010110010110001101000010000010101010010110010110010110010110010110001101000010000010101010010110000010000010010110000010000010001101101010101010010110000010000010010110000010000010001101101010101010010110000010000010010110000010000010001101101010101010010110010110000010010110010110000010010110001101101010010110010110000010010110010110000010010110001101101010010110010110000010010110010110000010010110001101101010010110010110010110010110010110010110010110010110001101010110010110010110010110010110010110010110010110001101010110010110010110010110010110010110010110010110001101001101101010101010101010101010101010101010101010101010001101101010101010101010101010101010101010101010101010001101101010101010101010101010101010101010101010101010010110001101101010000010101010101010000010101010101010010110001101101010000010101010101010000010101010101010010110001101101010000010101010101010000010101010101010010110010110001101000010000010101010000010000010101010010110010110001101000010000010101010000010000010101010010110010110001101000010000010101010000010000010101010010110000010000010001101101010101010101010101010101010010110000010000010001101101010101010101010101010101010010110000010000010001101101010101010101010101010101010010110010110000010010110001101101010000010101010101010010110010110000010010110001101101010000010101010101010010110010110000010010110001101101010000010101010101010010110010110010110010110010110001101000010000010101010010110010110010110010110010110001101000010000010101010010110010110010110010110010110001101000010000010101010010110000010000010010110000010000010001101101010101010010110000010000010010110000010000010001101101010101010010110000010000010010110000010000010001101101010101010010110010110000010010110010110000010010110001101101010010110010110000010010110010110000010010110001101101010010110010110000010010110010110000010010110001101101010010110010110010110010110010110010110010110010110001101010110010110010110010110010110010110010110010110001101010110010110010110010110010110010110010110010110001101001101101010101010101010101010101010101010101010101010001101101010101010101010101010101010101010101010101010001101101010101010101010101010101010101010101010101010010110001101101010000010101010101010000010101010101010010110001101101010000010101010101010000010101010101010010110001101101010000010101010101010000010101010101010010110010110001101000010000010101010000010000010101010010110010110001101000010000010101010000010000010101010010110010110001101000010000010101010000010000010101010010110000010000010001101101010101010101010101010101010010110000010000010001101101010101010101010101010101010010110000010000010001101101010101010101010101010101010010110010110000010010110001101101010000010101010101010010110010110000010010110001101101010000010101010101010010110010110000010010110001101101010000010101010101010010110010110010110010110010110001101000010000010101010010110010110010110010110010110001101000010000010101010010110010110010110010110010110001101000010000010101010010110000010000010010110000010000010001101101010101010010110000010000010010110000010000010001101101010101010010110000010000010010110000010000010001101101010101010010110010110000010010110010110000010010110001101101010010110010110000010010110010110000010010110001101101010010110010110000010010110010110000010010110001101101010010110010110010110010110010110010110010110010110001101010110010110010110010110010110010110010110010110001101010110010110010110010110010110010110010110010110001101";
    assert(out.str() == exp.str());
cout << "OK" << endl;
}
