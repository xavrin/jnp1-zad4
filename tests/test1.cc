#include <typeinfo>
#include <cassert>
#include <iostream>
#include <vector>
#include <algorithm>


#include "holding.hh"

typedef add_comp<Accountancy, Exchange_office>::type small_company;
typedef add_comp<multiply_comp<Hunting_shop, 10>::type, multiply_comp<Exchange_office, 20>::type>::type money;
typedef add_comp<multiply_comp<Hunting_shop, 30>::type, multiply_comp<Exchange_office, 10>::type>::type guns;
typedef Company<-1, 0, 0> big;

int main() {
  Group<money> s2(20);
  Group<money> s3(60);

  std::cout << s2;
  Group<money> sa(s2);
  std::cout << sa;
  Group<money> sb(s3);
  std::cout << sb;
  Group<money> sc;
  sc = s3;
  std::cout << sc;
  Group<money> sd;
  sd = s3;
  std::cout << sd;
  {
    Group<money> s1(20), s2(10), s3(150);
    Group<money> s[] = {s1, s2, s3};
    std::sort(s, s+3);
  }
  {
    Group<big> s(1);
    s.set_acc_val(1);
    assert(s.get_value() == (unsigned int)(-1));
  }

  return 0;
}

