#!/bin/bash

cp holding.hh tests
cd tests
for i in test*.cc
do
    if g++ -Wall -std=c++11 $i -o temp && ./temp > temp.out && rm temp
    then
        output=${i/cc/out}
        result=1;
        if [ -f $output ]
        then
            if ! diff temp.out $output
            then
                result=0
            fi
        fi
        if [ "$result" == "1" ]
        then
            echo $i "OK"
        else
            echo $i "FAIL"
        fi
        rm temp.out
    fi
done
rm holding.hh
